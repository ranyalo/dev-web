Créer la base de données : 

CREATE DATABASE IF NOT EXISTS passageague; 

CREATE USER IF NOT EXISTS passageague@localhost IDENTIFIED BY ‘********’; 

GRANT ALL ON passageague.* TO passageague@localhost; 

 

Créer les tables de la base de données : 

CREATE TABLE IF NOT EXISTS users ( id INTEGER(11) PRIMARY KEY AUTO_INCREMENT, 
                                                               prenom VARCHAR(255), 
                                                               nom VARCHAR(255), 
                                                               email VARCHAR(255), 
                                                               mot_de_passe VARCHAR(255), 
                                                               photo VARCHAR(255), 
                                                               admin VARCHAR(255) DEFAULT ‘user’); 


CREATE TABLE IF NOT EXISTS type ( id INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
                                                                lebel varchar(100));


CREATE TABLE IF NOT EXISTS cours_eau ( id INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
                                                                lebel varchar(100));


CREATE TABLE IF NOT EXISTS localisation ( id INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
                                                                lebel varchar(100));

CREATE TABLE IF NOT EXISTS equipement ( id INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
                                                                nom varchar(50),
                                                                detail varchar(500));
 

CREATE TABLE IF NOT EXISTS passage (id INTEGER(11) PRIMARY KEY AUTO_INCREMENT, 
                                                                    gps_x FLOAT, 
                                                                    gps_y FLOAT,  
                                                                    frequence_sub INTEGER(11),  
                                                                    longeur FLOAT,  
                                                                    largeur FLOAT,  
                                                                    trafic_journalier INTEGER(11),  
                                                                    taux_accidentalite INTEGER(11),
                                                                    photo varchar(255),  
                                                                    id_type_ouvrage INTEGER(11),  
                                                                    id_cours_eau INTEGER(11),  
                                                                    id_localisation INTEGER(11),  
                                                                    id_gestionaire INTEGER(11),  
CONSTRAINT users_passage FOREIGN KEY (id_gestionaire) REFERENCES users(id),
CONSTRAINT localisation_passage FOREIGN KEY (id_localisation) REFERENCES localisation(id),
CONSTRAINT cours_eau_passage FOREIGN KEY (id_cours_eau) REFERENCES cours_eau(id),
CONSTRAINT type_ouvrage_passage FOREIGN KEY (id_type_ouvrage) REFERENCES type(id));

CREATE TABLE IF NOT EXISTS passequip ( id_passage INTEGER(11),
                                       id_equipement INTEGER(11),
PRIMARY KEY (id_passage, id_equipement),
CONSTRAINT passequip_passage FOREIGN KEY (id_passage) REFERENCES passage(id),
CONSTRAINT passequip_equipement FOREIGN KEY (id_equipement) REFERENCES equipement(id));



CREATE TABLE IF NOT EXISTS procedures ( id INTEGER(11) PRIMARY KEY AUTO_INCREMENT,
                                                                nom varchar(50),
                                                                detail varchar(500),
                                                                date timestamp,
                                                                id_passage int,
CONSTRAINT passage_procedures FOREIGN KEY (id_passage) REFERENCES passage(id));


INSERT INTO cours_eau (id, label) VALUES
                                  ('', 'Torrents'),
                                  ('', 'Ruisseaux'),
                                  ('', 'Rivières'),
                                  ('', 'Fleuves');


INSERT INTO localisation (id, label) VALUES
                                     ('', '04 - Alpes-de-Haute-Provence'),
                                     ('', '05 - Hautes-Alpes'),
                                     ('', '06 - Alpes-Maritimes'),
                                     ('', '13 - Bouches-du-Rhône'),
                                     ('', '83 - Var'),
                                     ('', '84 - Vaucluse');


INSERT INTO type (id, label) VALUES
                             ('', 'Pierres'),
                             ('', 'Sols'),
                             ('', 'Chaussée immergée'),
                             ('', 'Pierres de gué'),
                             ('', 'Radier maçonnée'),
                             ('', 'Chaussée béton'),
                             ('', 'Chaussée asphaltée');







