<?php
require_once ("traitment.php");
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Ce site est le Portfolio de Rany ALO">
    <meta name="keywords" content="portfolio, Rany, Alo, cv, développeur web">
    <title>Portfolio Rany Alo</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <a id="start"></a>
    <label for="ch" id="lab"></label>
    <input type="checkbox" name="ch" id = "ch">
    <nav id="navbar">
        <ul>
            <li><a href="#accueil">Accueil</a></li>
            <li><a href="#apropos">A propos</a></li>
            <li><a href="#portfolio">Portfolio</a></li>
            <li><a href="#contact">Contact</a></li>
        </ul>
    </nav>

    <section id="accueil">
            <h1 id="heading">Rany<a style="color: brown;">Alo</a> 
                <br>Développeur <a style="color: brown;"> Web</a></h1>
            <a href="#start" class = "flech">
               <img src="images/fleche.svg" alt="fleche" class = "fleche">
            </a>
    </section>
    <section id="apropos">
        <h1 class="headings">A propos de moi</h1>
        <div id="about">
        <img src="images/img1.jpg" alt="aboutme" class="image">
        </div>
        <div class="row">
            <div class="box">
                <h3 class="title">Compétences Coding</h3>
                <div class="progress">
                    <h3> html <span>95%</span> </h3>
                    <div class="bar"><span></span></div>
                </div>
                <div class="progress">
                    <h3> css <span>80%</span> </h3>
                    <div class="bar"><span></span></div>
                </div>
                <div class="progress">
                    <h3> js <span>65%</span> </h3>
                    <div class="bar"><span></span></div>
                </div>
                <div class="progress">
                    <h3> php <span>75%</span> </h3>
                    <div class="bar"><span></span></div>
                </div>
            </div>
    
            <div class="box">
                <h3 class="title">Compétences professionnelles</h3>
                <div class="progress">
                    <h3> Web design <span>50%</span> </h3>
                    <div class="bar"><span></span></div>
                </div>
                <div class="progress">
                    <h3>Développement web<span>67%</span> </h3>
                    <div class="bar"><span></span></div>
                </div>
                <div class="progress">
                    <h3>Photoshop<span>50%</span> </h3>
                    <div class="bar"><span></span></div>
                </div>
                <div class="progress">
                    <h3>WordPress<span>70%</span> </h3>
                    <div class="bar"><span></span></div>
                </div>
            </div>
    
            <div class="box">
                <h3 class="title">Experiences</h3>
                <div class="exp-box">
                    <h3>Stage Cerema Marseille, France</h3>
                    <p>De septembre 2021 à octobre 2021. J'ai réalisé un site web qui permet d'identiﬁer et localiser
                        les passages à gué en grand nombre dans la région sud de
                        la France.</p>
                </div>
                <div class="exp-box">
                    <h3>Ingénieur automatisme-instrumentation</h3> 
                        <p>Syrian Petroleum Company (SPC) Rmailan - Syrie 2005-2016</p>
                    <div class="exp-box">
                        <h3>2012/2016 :</h3>
                        <p> Responsable de l’Atelier « Instruments
                            électriques et électroniques ».</p>
                    </div>
                    <div class="exp-box">
                        <h3>2010/2012 :</h3>
                        <p>Responsable de l’Atelier « Instruments
                            Pneumatiques ».</p>
                    </div>
                    <div class="exp-box">
                        <h3>2005/2010 :</h3>
                        <p>Ingénieur Instrumentation Junior
                            Exploitation, Field et stockage.</p>
                    </div>
                </div>
            </div>
    
            <div class="box">
                <h3 class="title">Education</h3>
                <div class="exp-box">
                    <h3>Titre Professionnel Développeur Web et Web Mobile</h3>
                    <p>Simplon.co PACA Marseille 13, France.<br>
                        Depuis mai 2021</p>
                </div>
                <div class="exp-box">
                    <h3>Français, Anglais, Vivre en France</h3>
                    <p>Wintegreat (Sciences Po-Aix) Aix-en-
                        Provence, France.<br>
                        De février 2019 à mai 2019</p>
                </div>
                <div class="exp-box">
                    <h3>Langue Française, DELF B1</h3>
                    <p>Sud Formation Marseille 13, France.<br>
                        De septembre 2018 à décembre 2018</p>
                </div>
                <div class="exp-box">
                    <h3>Master en Automatisme et Electronique Industrielle</h3>
                    <p>Université d'Alep Syrie.<br>
                        De 1998 à 2003.<br>
                        Titre validé par l’ENIC NARIC.</p>
                </div>
            </div>
    
        </div>
        </section>
        <section id="portfolio">
            <h1 class="headings">Portfolio</h1>
            <div id="contenu">
                <div id="un">
                    <a href="http://passageague.alwaysdata.net/">
                    <div class="info">
                        <h3>PASSAGE À GUÉ</h3>
                        <p>Une application web permettant d'identifier et localiser les passages à gué en grand nombre dans les régions sud</p>
                    </div>  
                    </a> 
                    <img src="images/passageague.png" alt="projet1">             
                </div>
                <div id="deux">
                    <a href="https://www.figma.com/file/vOYvO743C8NvnepltiCy6H/passageague">
                    <div class="info">
                        <h3>Maquettage site Web</h3>
                        <p>La maquette est une méthode de conception d'interfaces qui nous permet de vous proposer des interfaces répondant à vos attentes et besoins.</p>
                    </div> 
                    </a> 
                    <img src="images/maquette.png" alt="projet2">    
                </div>
                <div id="trois">
                    <a href="https://ssvp-marseille.alwaysdata.net/">      
                    <div class="info">
                        <h3>Site en WordPress</h3>
                        <p>Le système de gestion de contenu (SGC) ou Content Management System (CMS)</p>
                    </div> 
                    </a>  
                    <img src="images/wordpress.png" alt="projet3">
                </div>
                <div id="quatre">
                    <a href="simulateur/simulateur.html">   
                    <div class="info">
                        <h3>Simulateur de coût</h3>
                        <p>Projet Hackathon pour 'COMMENT'</p><br>
                        <p>Ce simulateur a pour objectif de donner une fourchette travaux et une estimation de coût aux agents immobiliers et aux clients</p>
                    </div>
                    </a>
                    <img src="images/simulateur.png" alt="projet4">
                </div>
            </div>
        </section>
    <section id="contact">
            <h1 class="headings">Contactez Moi</h1>
        <div id="container">
         <div id="cont">
            <form method="post">
                <label for="nom" id = "lnom">Votre nom</label>
                <input type="text"  id="nom" name = "name" required><br>
                <label for="email">Votre Email</label>
                <input type="email"   id="email" name = "email" required><br>
                <label for="message">Votre Message</label>
                <textarea  id = "message"  name = "message" required></textarea><br>
                <button type="submit" name = "envoyez" id = "btn">Envoyez</button>
                <div id="statusMessage"> 
                    <?php if (! empty($mail_msg)) { ?>
                      <p><?php echo $mail_msg; ?></p>
                    <?php } ?>
                    </div>
            </form>
         </div>
         <div class="cont1">
                <div class="box">
                    <img src="images/telephone.png" alt="téléphone">
                    <h3>Mon téléphone</h3>
                    <p>+33-601294761</p>
                </div>
                <div class="box">
                    <img src="images/email.png" alt="email">
                    <h3>Mon email</h3>
                    <p>ranygabroalo@gmail.com</p>
                </div>
                <div class="box">
                    <img src="images/adresse-du-domicile.png" alt="">
                    <h3>Mon address</h3>
                    <p>70 Rue Bernard du bois</p><b><p>13001 Marseille</p>
                </div>
                <a href="https://gitlab.com/ranyalo/dev-web/">
                <div class="box">
                    <img src="images/git.png" alt="">
                    <h3>Dépôt Git</h3>
                    <p>Accédez</p>
                </div></a>
                <a href="CV_Rany_ALO.pdf">
                <div class="box">
                    <img src="images/cv.png" alt="">
                    <h3>Curriculum Vitae</h3>
                    <p>Téléchargez</p>
                </div></a>
                <a href="https://www.linkedin.com/in/rany-alo-8b6559162/">
                <div class="box">
                    <img src="images/linkedin.png" alt="">
                    <h3>Compte linkedin</h3>
                    <p>Accédez</p>
                </div></a>
            </div>
         </div>
    </section>
        <section id = "footer">
            <div id = "footer1">
                <p>Portfolio RANY ALO 2021</p>
            </div>
        </section>
    
</body>
</html>