# Les conditions et les boucles 

## Exercice 1 

Afficher les nombres pairs de 20 à 0. 

```js
for(let i = 20; i >=0 ; i= i - 2)  {
    console.log(i)
}
```

## Exercice 2 

Dans une boucle, générer un nombre entier aléatoire compris entre 0 et 100. 
Lorsque le nombre vaut 42, vous quitterez la boucle et afficherez le nombre de tours qu'il aura fallu pour tirer le nombre 42.

Modifiez le programme pour tirer 100 fois le nombre 42. Vous afficherez la moyenne du nombre de tirage nécessaire pour afficher le nombre 42

```js 
// On crée une variable qui va contenir le résultat du tirage
let tirage = 0;

// ON crée une variable qui permet de compter les tirages 
let nombre = 0;

let moyenne = 0;

// On crée un variable qui stocke le nombre total de tirage
// on doit boucler 100 fois
for(let i = 0 ; i < 100 ; i++){
    // On boucle tant que le tirage est différent de 42
    while(tirage != 42 ) {
        //On effectue un tirage aléatoire
        tirage = Math.floor(Math.random()*100)
        //console.log(tirage)
        nombre++;
    }
    console.log(`Il y a eu ${nombre} tirages pour obtenir 42 au tour ${i}`)
    tirage = 0;
    moyenne = moyenne + nombre; 
    nombre = 0;  
}
moyenne = moyenne / 100;
console.log(`Il faut en moyenne ${moyenne} tirages pour obtenir 42` )

```

```js
// Version proposée par Hugo : 
let nombre42 = 0; 
let tirage = 0;
let compteur = 0; 

while (nombre42 < 100) {
    
    tirage = Math.floor(Math.random()*100); 
    if(tirage == 42) {
        nombre42 ++;
    }
    compteur ++; 
}
let moyenne = compteur / 100; 
console.log(`Il faut en moyenne ${compteur} tirages pour obtenir 42`)

```


## Exercice 3

Calculer la factorielle du nombre 7. 
Modifier le programme pour qu'il calcule les factorielles de 1 à 100. Quand est-ce que le programme **plante** ? (Jamais, node gère bien)

```js 
let factorielle = 1; 
for(let nombre = 1; nombre <= 100; nombre ++){
    for (let i = 2; i <= nombre; i++) {
        factorielle = factorielle * i; 
    }
    console.log(`La factorielle de ${nombre} vaut ${factorielle}`)
    factorielle = 1;
}
```

## Exercice 4

Afficher les nombres premiers de 0 à 100. 
Un nombre est **premier** s'il n'est divisible que par 1 et lui-même. 

```js 
let estDivise = false; 

for(let nombre = 2; nombre <= 100; nombre ++ ){
    for(let i = 2; i <= Math.sqrt(nombre) && !estDivise ; i ++) {
        // MAth.sqrt permet d'obtenir la racine carré du nombre passé en paramètre (Square root)
        if(nombre % i == 0) {
            estDivise = true; 
        }
    }
    if(estDivise == false) {
        console.log(`${nombre} est premier`)
    }
    estDivise = false;
}
```


## Exercice 5 

Écrire une fonction `diviseurs` qui renvoi un tableau contenant tous les nombres qui sont les diviseurs d'un nombre donné en paramètre : 

```js
    /**
     * Renvoi un tableau contenant tous les diviseurs du nombre donné en paramètre
     * 
     * @param {Number} nombre 
     * @returns 
     */
    function diviseurs(nombre){
        let diviseurs = [];
        for(let i = 1; i < nombre; i++ ) {
            if(nombre % i == 0 ) {
                diviseurs.push(i); 
            }
        }
        return diviseurs; 
    }
```