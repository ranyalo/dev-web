
/**
 * Indique si l'année est bissextile
 * 
 * Une année est bissextile si 
 *  - elle est divisible par 4, mais pas par 100
 *  - ou bien qu'elle est divisible par 400
 * Ainsi, 1900 n'est pas bissextile, mais 2000 oui. 
 * 
 * @param {Number} annee 
 * @returns boolean true si l'année est bissextile, false si non.  
 */
//function bissextile(annee) {}

// On initialise une variable `annee` avec l'année à tester

// on affiche le nombre de jours de l'année : 
// 366 si l'année est bissextile, 365 si elle ne l'est pas : 
// Exemple : l'année 2000 a 366 jours.  

const { exit } = require("process");
const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question("Veuillez entrer une annee ", (answer) => bissextile(answer));
function bissextile (annee) {
    let n = annee;
    if (n%4 == 0 && n%100 != 0 || n%400 == 0) {
        console.log(`Annee ${n} est bissextile et a 366 jours`);
        return true;
        }
    else {
        console.log(`L'annee ${n} n'est pas bissextile et a 365 jours`);
        return false;
    }
    
}
