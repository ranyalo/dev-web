var player1 = 'Toto';
var player2 = 'Titi';
// Pour multiplier par 3 la durée d'un combat, on peut multiplier la vie des joueurs par  3 :
// var player1Life = 100 * 3;
// var player2Life = 100 * 3;
var player1Life = 100;
var player2Life = 100;

console.clear();
console.log('/////////////////////////////////////');
console.log('  C\'est l\'heure du dueeeeeeeeel !');
console.log(`/////////////////////////////////////`);
console.log('');
while (player1Life > 0 && player2Life > 0) {
    // Pour multiplier par 3 la durée d'un combat, on peut aussi diviser la puissance d'attaque par 3 :
    // var puissanceAttaque1 = Math.floor(Math.random() * 50) / 3;
    // var puissanceAttaque2 = Math.floor(Math.random() * 50) / 3;
    let puissanceAttaque1 = Math.floor(Math.random() * 50);
    let puissanceAttaque2 = Math.floor(Math.random() * 50);
    const essaieAttaque1 = Math.floor(Math.random() * 10 + 1); // On obtient un nombre entre 1 et 10 
    const essaieAttaque2 = Math.floor(Math.random() * 10 + 1); 
    const potion1 = Math.floor(Math.random() * 60 + 30);
    const potion2 = Math.floor(Math.random() * 60 + 30);
    console.log('');
    console.log(`${player1} (${player1Life})  -  ${player2} (${player2Life})`);
    
    
    if(player1Life < 20 ) {
        player1Life = player1Life + potion1;
        console.log(`${player1} prend une potion de ${potion1} points de vie, il a maintenant ${player1Life} points`);
    } else {
        if(essaieAttaque1 > 1) { // 9 chances sur 10 pour que la valeur soit > 1 
            console.log(`${player1} attaque et enlève ${puissanceAttaque1} points de vie`);
            player2Life = player2Life - puissanceAttaque1;
        } else {
            console.log(`${player1} a raté son attaque`);
        }
    }
    // Pour que le jeu soit plus juste, il faut empêcher le joueur2 d'attaquer s'il n'a plus de vie
    if(player2Life < 20 ) {
        player2Life += potion2;
        console.log(`${player2} prend une potion de ${potion2} points de vie, il a maintenant ${player2Life} points`);
    } else {
        if(player2Life > 0 ){
            if(essaieAttaque2 < 10) { // 9 chances sur 10 pour que la valeur soit < 10 
                console.log(`${player2} attaque et enlève ${puissanceAttaque2} points de vie`);
                player1Life = player1Life - puissanceAttaque2;  
            } else {
                console.log(`${player2} a raté son attaque`);
            }
        }
    }
}

console.log("/////////////////////////////////////");
// console.log(player1Life > 0 ? `${player1} est vainqueur` : `${player2} est vainqueur`);

// Pour plus de justice, on teste les 3 cas, avec une ternaire imbriquée :
// console.log(player1Life > 0 ? `${player1} est vainqueur` : player2Life>0 ? `${player2} est vainqueur`: 'Match nul');

// Pour plus de justice, on teste les 3 cas, avec un if/else if /else :
if (player1Life > 0) {
    console.log("Joueur 1 a gagné")
}
else if (player2Life > 0) {
    console.log("Joueur 2 a gagné")
}
else {
    console.log('ils sont tous nuls !!!!')
}

/*
Ces lignes permettent de vérifier si le joueur 1 a encore de la vie
et d'afficher qu'il a gagné si c'est le cas.
*/
if (player1Life > 0) {
    console.log("C'est un peu comme si c'est toi le joueur 1, alors bravo (même si tu n'as rien fait...).")
}
