<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ssvp' );

/** MySQL database username */
define( 'DB_USER', 'ssvp' );

/** MySQL database password */
define( 'DB_PASSWORD', 'r1r2r3r4' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '%zzX/??6kQ0Ghqj9|F}nI(_.m3}S;kj$].)!3[`UMI}ok$.a/m+i)v#]gzLKS:M)' );
define( 'SECURE_AUTH_KEY',  'O6_N4*rNK%p-w$,_,Zt=i]C#(k8No/m=F>d&[RTTH}biL|IcU5+}[3h=a{ba+9_l' );
define( 'LOGGED_IN_KEY',    '9V1cAMiien<YwvY}Ae44:[ls;[yy3aP23lw]P=m.2[}5L8X$nXPke*+]u|3XpuPx' );
define( 'NONCE_KEY',        'AQPjRgq]ySE?%09!f{}},S0Z#X;j]Vhk|+0BDj+vzf[8[7xpxg:R;3{~jb@#B{~#' );
define( 'AUTH_SALT',        'b17*bKp,U<jfun~SreW]t*[H(S)-zg~3QJ+!eI8oQY]njJl p5m!OMK@LEW[dOL(' );
define( 'SECURE_AUTH_SALT', 'gxY4mldvmL4_^B t#R`SVgDjP,}v,FhY kxq_da6%DpI!>z^ZrDm: 8vt7kAxY:M' );
define( 'LOGGED_IN_SALT',   'HCQQRE)6AiIV_-!1S[7lMOeiF<N0px)cYg,_]Q(._.F5J+<4jdo<=jzQQ@p2$n,L' );
define( 'NONCE_SALT',       't4Hw?dZ.HBifp.KIO3CC!#D$jk52.S3/(,e>t6&N_Zc`[]kJ8fWCxt)-sJR5d6*L' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
