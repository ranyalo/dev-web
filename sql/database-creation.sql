/* Table 'users' */
CREATE TABLE users (
    id serial NOT NULL,
    login varchar(20) NOT NULL,
    "password" varchar(90),
    phone varchar(15),
    mail varchar(90),
    PRIMARY KEY(id)
);

/* mariadb*/
create table users(
user_id int auto_increment,
login varchar(20) not null,
password varchar(90),
phone varchar(15),
mail varchar(90), 
primary key(user_id));


/* Table 'tasks' */
CREATE TABLE tasks (
    id serial NOT NULL,
    "label" varchar(50) NOT NULL,
    done bool NOT NULL,
    creation_date timestamp,
    users_id serial NOT NULL,
    PRIMARY KEY(id)
);

/* Relation 'users-tasks' */
ALTER TABLE tasks ADD CONSTRAINT "users-tasks"
FOREIGN KEY (users_id)
REFERENCES users(id);

