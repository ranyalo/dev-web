# Exercices de sélection sur la base employees. 

Le schéma décrivant la base se trouve ici : https://dev.mysql.com/doc/employee/en/sakila-structure.html

Vous pouvez accéder à la base à l'adresse suivante : 
```sh
PASSWORD=pipou100f00rm47ion psql --host=asciiparait.fr --port 8131 -U courssqldebutant --dbname employes
```

## Requêtes d'interrogation simples. 
Effectuez les requêtes suivantes : 

 1. Afficher le numéro, le nom, le prénom, le genre et la date de naissance de chaque employé. ( emp_no, last_name, first_name,  gender, birth_date )
 ```sql
 select emp_no, last_name, first_name,  gender, birth_date from employees;
 ```
 2. Trouver tous les employés dont le prénom est 'Troy'. ( emp_no, first_name, last_name, gender )
 ```sql
 select emp_no, first_name, last_name, gender from employees where first_name like '%Troy';
 ```
 3. Trouver tous les employés de sexe féminin ( * ).
 ```sql
 select * from employees where gender = 'F';
 ```
 4. Trouver tous les employés de sexe masculin nés après le 31 janvier 1965 ( * ) ?
 ```sql
 select * from employees where gender = 'M' and birth_date > '1965-01-31';
 ```
 5. Lister uniquement les titres des employés, sans que les valeurs apparaissent plusieurs fois. (title) ?
 ```sql
 select distinct title from titles;
 ```
 6. Combien y a t'il de département ? ( nombreDep ) ?
 ```sql
 select count(dept_no) as nombreDep from departments;
 ```
 7. Qui a eu le salaire maximum de tous les temps, et quel est le montant de ce salaire ? (emp_no, maxSalary) ?
 ```sql
 select salary as maxSalary,emp_no from salaries order by salary desc limit 1;
 ```
 8. Quel est salaire moyen de l'employé numéro 287323 toute période confondue ?  (emp_no, salaireMoy) ?
 ```sql
 select emp_no, AVG(salary) as salaireMoy from salaries where emp_no = 287323 group by emp_no;
 ```
 9. Qui sont les employés dont le prénom fini par 'ard' (*) ?
 ```sql
 select * FROM employees where first_name like '%ard';
 ```
 10. Combien de personnes dont le prénom est 'Richard' sont des femmes ?
 ```sql
 select COUNT(first_name) FROM employees WHERE first_name = 'Richard' and gender = 'F';
 ```
 11. Combien y a t'il de titre différents d'employés (nombreTitre) ?
 ```sql
 select count(distinct title) as nombreTitre FROM titles;
 ```
 12. Dans combien de département différents a travaillé l'employé numéro 287323 (nombreDep) ? 
 ```sql
 select COUNT(distinct dept_no) as nombreDep FROM dept_emp where emp_no = '287323';
 ```
 13. Quels sont les employés qui ont été embauchés en janvier 2000. (*) ? Les résultats doivent être ordonnés par date d'embauche chronologique
 ```sqm
 select * FROM employees where hire_date between '2000/01/01' and '2000/01/31' order by hire_date asc;
 ```
 14. Quelle est la somme cumulée des salaires de toute la société (sommeSalaireTotale) ?
 ```sql
 select sum(salary) as sommeSalaireTotale FROM salaries;
 ```

## Requêtes avec jointures :
 15. Quel était le titre de Danny Rando le 12 janvier 1990 ? (emp_no, first_name, last_name, title) ?
 ```sql
 select employees.emp_no, employees.first_name,employees.last_name,titles.title FROM employees  join titles on titles.emp_no = employees.emp_no where employees.first_name = 'Danny' and employees.last_name = 'Rando' and '1990/01/12' between titles.from_date and titles.to_date;
 ```
  
 16. Combien d'employés travaillaient dans le département 'Sales' le 1er Janvier 2000 (nbEmp) ? 
 ```sql
select COUNT(dept_emp.emp_no) as nbEmp FROM dept_emp inner join departments ON dept_emp.dept_no = departments.dept_no where departments.dept_name = 'Sales' and '2000/01/01' BETWEEN dept_emp.from_date AND dept_emp.to_date;
 ```
 17. Quelle est la somme cumulée des salaires de tous les employés dont le prénom est Richard (emp_no, first_name, last_name, sommeSalaire) ?
 ```sql
 select employees.emp_no, employees.first_name, employees.last_name, SUM(salary) as sommeSalaire from employees inner join salaries ON salaries.emp_no = employees.emp_no where employees.first_name like 'Richard' group by employees.emp_no, employees.first_name,employees.last_name;
 ```

## Agrégation
 19. Indiquer pour chaque prénom 'Richard', 'Leandro', 'Lena', le nombre de chaque genre (first_name, gender, nombre). Les résultats seront ordonnés par prénom décroissant et genre. 
 ```sql
 select first_name, gender, count(gender) as nombre FROM employees where first_name = 'Richard' or first_name = 'Leandro' or first_name = 'Lena' group by first_name, gender order by first_name desc, gender; 
 ```
 20. Quels sont les noms de familles qui apparaissent plus de 200 fois (last_name, nombre) ? Les résultats seront triés par leur nombre croissant et le nom de famille.
```sql
 select last_name, COUNT(last_name) as nombre from employees group by last_name  having count(last_name)  > '200' order by last_name asc,nombre; 
 ```
  
 21. Qui sont les employés dont le prénom est Richard qui ont gagné en somme cumulée plus de 1 000 000 (emp_no, first_name, last_name, hire_date, sommeSalaire) ?
 ```sql
 select employees.emp_no, employees.first_name, employees.last_name, employees.hire_date, sum(salary)as sommeSalaire from employees  inner join salaries ON salaries.emp_no = employees.emp_no where employees.first_name like 'Richard' group by employees.first_name,employees.last_name,employees.emp_no,employees.hire_date having SUM(salaries.salary) > '1000000'; 
 ```
 22. Quel est le numéro, nom, prénom  de l'employé qui a eu le salaire maximum de tous les temps, et quel est le montant de ce salaire ? (emp_no, first_name, last_name, title, maxSalary)
 bonus. Qui est le manager de Martine Hambrick actuellement et quel est son titre (emp_no, first_name, last_name, title) 
```sql
 select employees.emp_no, employees.first_name, employees.last_name, titles.title,salaries.salary as maxSalary FROM employees inner join salaries ON salaries.emp_no = employees.emp_no inner join titles ON titles.emp_no = employees.emp_no  order by salaries.salary desc limit 1;
 ```
 ```sql
 select employees2.emp_no, employees2.first_name, employees2.last_name, titles.title from employees 
        join dept_emp on employees.emp_no = dept_emp.emp_no 
        join departments on dept_emp.dept_no = departments.dept_no 
        join dept_manager on departments.dept_no = dept_manager.dept_no 
        join employees as employees2 on dept_manager.emp_no = employees2.emp_no JOIN titles  ON titles.emp_no  =  employees2.emp_no where dept_manager.to_date = '9999-01-01' and titles.to_date = '9999-01-01' and employees.first_name = 'Martine' and employees.last_name = 'Hambrick';

 SELECT m.emp_no, m.first_name, m.last_name, t.title
    FROM employees e 
        NATURAL JOIN dept_emp de 
        NATURAL JOIN departments d 
        JOIN dept_manager dm ON d.dept_no = dm.dept_no 
        JOIN employees m ON dm.emp_no = m.emp_no   
        JOIN titles t ON m.emp_no = t.emp_no  
    WHERE e.first_name = 'Martine' and e.last_name = 'Hambrick' and NOW() BETWEEN dm.from_date AND dm.to_date AND NOW() BETWEEN t.from_date AND t.to_date ;
 ```

## La suite : 
 23. Quel est actuellement le salaire moyen de chaque titre  (title, salaireMoyen) ? Classé par salaireMoyen croissant
 ```sql
 SELECT title, AVG(salary) AS salaireMoyen
 FROM titles t INNER JOIN employees e ON e.emp_no = t.emp_no INNER JOIN salaries s ON s.emp_no = e.emp_no
 WHERE now() BETWEEN t.from_date AND t.to_date AND now() BETWEEN s.from_date AND s.to_date
 GROUP BY title; 
 ```
 24. Combien de manager différents ont eu les différents départements (dept_no, dept_name, nbManagers), Classé par nom de département
  ```sql
select departments.dept_no, departments.dept_name, COUNT(dept_manager.emp_no) as  nbManagers from departments JOIN dept_manager ON dept_manager.dept_no = departments.dept_no group by departments.dept_no, departments.dept_name order by departments.dept_name;
 ```
 25. Quel est le département de la société qui a le salaire moyen le plus élevé (dept_no, dept_name, salaireMoyen)

 ```sql
SELECT departments.dept_no, departments.dept_name, AVG(salaries.salary) AS "salaireMoyen" FROM departments JOIN dept_emp ON dept_emp.dept_no = departments.dept_no JOIN salaries ON dept_emp.emp_no = salaries.emp_no GROUP BY departments.dept_no, departments.dept_name ORDER BY AVG(salaries.salary) DESC LIMIT 1;

                                                                  
 ```
26. Quels sont les employés qui ont eu le titre de 'Senior Staff' sans avoir le titre de 'Staff' ( emp_no , birth_date , first_name , last_name , gender , hire_date )

  ```sql
SELECT e.emp_no, e.first_name, e.last_name 
FROM employees AS e JOIN titles AS t ON e.emp_no = t.emp_no 
WHERE t.title='Senior Staff' AND 
e.emp_no NOT IN (SELECT emp_no from titles where title = 'Staff'); 
 ```
 27. Indiquer le titre et le salaire de chaque employé lors de leur embauche (emp_no, first_name, last_name, title, salary)

  ```sql
select employees.emp_no, employees.first_name, employees.last_name, titles.title, salaries.salary from employees JOIN salaries ON salaries.emp_no = employees.emp_no JOIN titles ON titles.emp_no = employees.emp_no where employees.hire_date = salaries.from_date AND employees.hire_date = titles.from_date;

 ```
 28. Quels sont les employés dont le salaire a baissé (emp_no, first_name, last_name)

  ```sql
SELECT e.emp_no, first_name, last_name  from employees AS e JOIN salaries AS s ON e.emp_no = s.emp_no  WHERE EXISTS (select * from salaries s2 where s.emp_no = s2.emp_no and s.salary < s2.salary and s.from_date > s2.from_date);
 ```
