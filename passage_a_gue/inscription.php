<?php

require_once "libraries/utiles.php";
require_once "libraries/models/Users.php";
require_once "libraries/models/Model.php";
$modelUser = new Users();

if (isset($_REQUEST['inscription']))  // 'inscription' le nom de bouton
{          
     // prendre les informations saisi par l'utilisateur est les mettre dans des variables  
   $firstname = strip_tags($_REQUEST['prenom']);
   $lastname = strip_tags($_REQUEST['nom']);
   $email = strip_tags($_REQUEST['email']);
   $password = strip_tags($_REQUEST['password']);
   $password_v = strip_tags($_REQUEST['password_v']);
   $errorMsg =  verfication($lastname, $email, $password, $password_v, $extension, $extensions, $size, $maxSize, $error, $tmpName, $name);
   if (!isset($errorMsg))
   {	
     try
      {	
        $row = $modelUser->selectuser($lastname, $email); // appeler le fonction de selectioner une utilisateur	
        $id = $row['id'];
        if($row["email"]==$email){
            $errorMsg[]="Email est dèja existe";	//check condition email est dèja existe 
        }
        else
        {
            $file = $modelUser->uploadimage($id, $name, $extension, $tmpName); // inserer la photo de profile

            $new_password = password_hash($password, PASSWORD_DEFAULT); //encrypt le mot de passe en utilisant password_hash()
        
            $modelUser->insertuser($firstname, $lastname, $email, $new_password, $file);
            $registerMsg="Inscription réussie..... Veuillez cliquer sur le lien du 'Se connecter'"; //execute query success message
            
        }
       }
       catch(PDOException $e)
       {
        echo $e->getMessage();
       }
   }
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Le site propose...">
    <meta name="keywords" content="Passage, Gue, Route">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Passag à gué</title>
    <!-- Render all elements normaly -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- font awsome library -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- main template css file -->
    <link rel="stylesheet" href="css/index.css">
    <!-- header -->
    <link rel="stylesheet" href="css/header.css">
    <!-- landing -->
    <link rel="stylesheet" href="css/landing.css">
    <link rel="stylesheet" href="css/inscription.css">
    <!-- main heading  -->
    <link rel="stylesheet" href="css/main-heading.css">
    <!-- portfolio -->
    <link rel="stylesheet" href="css/portfolio.css">
    <!-- footer -->
    <link rel="stylesheet" href="css/footer.css">
    <!-- Google font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
    
    
</head>

<body>
    <header>
        <div class="container">
            <a href="" class="logo">
                <img src="images/logo.png" alt="logo">
            </a>
            <nav>
                <i class="fas fa-bars menu"></i>
                <ul>
                    <li><a href="index.php" class="home">Home</a></li>
                    <li><a href="index.php#contact" class="Contact">Contact</a></li>
                    <li><a href="inscription.php" class="inscription">Inscription</a></li>
                    <li><a href="login.php" class="connection">Connection</a></li>
                </ul> 
            </nav>
        </div>
    </header>
    
    <div id="landing">
    <div class="container1">			
       <?php 
         if(isset($errorMsg))
          {
               foreach($errorMsg as $error)
               {
              ?>
                 <div class="alert-danger">
                     <strong>ERREUR ! <?php echo $error ; ?></strong>
                </div>
             <?php
               }
          }
        if(isset($registerMsg))
          {
             ?>
            <div class="alert-success">
                  <strong><?php echo $registerMsg; ?></strong>
              </div>
            <?php
          }
            ?> 
            
    <form method="post" enctype = "multipart/form-data">
      <div id="creer">
          <h4>CRÉER MON COMPTE</h4>
          <hr><br/>
      </div>
    
          <label class="nom" for="nom">Nom:</label>
          <label class="prenom1" for="prenom">Prénom:</label>
          <input class="name" type="text" name="nom" placeholder="Nom">
          <input class="prenom" type="text" name="prenom" placeholder="Prénom">
          <label class="email1" for="email">Email:</label>
          <input class="email" type="email" name="email" placeholder="Email">
          <label class="pass" for="">Mot de passe:</label>
          <input class="password" type="password" name="password" placeholder="Mot de passe" >
          <label class="pass1" for="mot de passe">Confirmation:</label>
          <input class="password1" type="password" name="password_v" placeholder="Confirmation" >
          <label class="photo" for="photo">Photo:</label>         
          <input class="file" type="file" name="userimage">
          <input class = "submit" type="submit" name="inscription" value="Inscription">
          <hr>
      <div id = "connection">
          <a href="login.php">Se connecter</a>
      </div>
    </form>
</div> 
</div>
<footer>
    <div class="footer">
        <div>
            <a href="mentionsLegales.html" target = "_blank" class="btn btn-secondary bouton">Mentions légales</a>
        </div>
            <br>
        <span> 
            &copy; 2021 Copyright Cerema & Simplon
        </span>
        </div>
</footer> 
</body>
</html>