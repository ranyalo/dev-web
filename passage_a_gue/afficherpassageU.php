<?php

require_once "libraries/models/Users.php";
require_once "libraries/models/Passages.php";

$modelUser = new Users();
$modelPassage = new Passages();

session_name('user_login');
session_start();
if (!isset($_SESSION["user_login"])) {
    header("Location: index.php");
}
$id = $_SESSION["user_login"];
$user = $modelUser->selectUserByid($id);


// Récupération du param "id" et vérification de celui-ci
$id_passage = null;
// On verifie si il y'en a un et que c'est un nombre entier.
if (!empty($_GET['id_passage']) && ctype_digit($_GET['id_passage'])) {
    $id_passage = $_GET['id_passage'];
}
if (!$id_passage) {
    die("Vous devez préciser un paramètre `id` dans l'URL !");
}
$passage = $modelPassage->selectPassageByid($id_passage);
        
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Le site propose...">
    <meta name="keywords" content="Passage, Gue, Route">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Passag à gué</title>
    <!-- Render all elements normaly -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- font awsome library -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- main template css file -->
    <link rel="stylesheet" href="css/index.css">
    <!-- header -->
    <link rel="stylesheet" href="css/header-user.css">
    <!-- main heading  -->
    <link rel="stylesheet" href="css/main-heading.css">
    <link rel="stylesheet" href="css/afficherpassage.css">
    <!-- footer -->
    <link rel="stylesheet" href="css/footer.css">
    <!-- Google font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
</head>

<body>
     <!-- Start Header -->
    <header>
        <div class="header-user">
            <div class="container">
                <img class="logo" src="images/logo.png" alt="">
                <h3>Bienvenu <?php echo $user['prenom']; ?></h3>
                <div class="links">
                    <nav>
                        <i class="fas fa-bars menu"></i>
                        <ul>
                            <li><a href="indexuser.php">Home</a></li>
                            <li><a href="userprofile.php">Profile</a></li>
                            <li><a href="indexuser.php#contact">Contact</a></li>
                            <li><a href="logout.php">Desconnection</a></li>
                        </ul>
                    </nav>
                </div>
                <?php echo "<img src='./upload/".$user['photo']."' width='70px' height = '70px' >"?>
            </div>
        </div>
    </header>
    <!-- End Header -->
    <!-- start landing -->
    <div id="landing">
    <div class="containerall">
            <h2>VOIR LE PASSAGE</h2>
            <div class="container1">
                <div id="cont-photo">
                <h3>Ajouter par : <?php echo $passage['prenom']." ".$passage['nom'];?> </h3>
                <?php echo "<img class = 'ges' src='./upload/passage/".$passage['photop']."' >"?>
                </div>
                <div id="cont">
                    <form method="post" enctype = "multipart/form-data">
                    <label class="gps_x1" for="gps_x1">Gps_x:</label>
                    <input class="gps_x" type="integer" name="gps_x" value = "<?php echo $passage['gps_x'];?>">
                    <label class="gps_y1" for="gps_y1">Gps_y:</label>
                    <input class="gps_y" type="integer" name="gps_y" value = "<?php echo $passage['gps_y'];?>">
                    <label class="frequence_sub1" for="frequence_sub1">Frequence sub:</label>
                    <input class="frequence_sub" type="integer" name="frequence_sub" value = "<?php echo $passage['frequence_sub'];?>">
                    <label class="longeur1" for="longeur1">Longeur:</label>
                    <input class="longeur" type="integer" name="longeur" value = "<?php echo $passage['longeur'];?>" >
                    <label class="largeur1" for="largeur1">Largeur:</label>
                    <input class="largeur" type="integer" name="largeur" value = "<?php echo $passage['largeur'];?>" >         
                </div>
                <div id="cont">
                    <label class="trafic_journalier1" for="trafic_journalier1">Trafic journalier:</label>
                    <input class="trafic_journalier" type="text" name="trafic_journalier" value = "<?php echo $passage['trafic_journalier'];?>">
                    <label class="taux_accidentalite1" for="taux_accidentalite1">Taux accidentalite:</label>
                    <input class="taux_accidentalite" type="integer" name="taux_accidentalite" value = "<?php echo $passage['taux_accidentalite'];?>">
                    <label class="type_ouvrage1" for="type_ouvrage1">Type ouvrage:</label>
                    <input class="type_ouvrage" type="text" name="type_ouvrage" value = "<?php echo $passage['type_ouvrage'];?>">
                    <label class="cours_eau1" for="cours_eau1">Cours eau:</label>
                    <input class="cours_eau" type="text" name="cours_eau" value = "<?php echo $passage['cours_eau'];?>">
                    <label class="localisation1" for="localisation1">Localisation:</label>
                    <input class="localisation" type="text" name="localisation" value = "<?php echo $passage['localisation'];?>">
                </div>
            </div>               
        </div>
    </div>
    
    <!--    End landing -->
    <footer>
        <div class="footer">
            <div>
            <a href="mentionsLegales.html" target="_blank" class="btn btn-secondary bouton">Mentions légales</a>
            </div>
            <br>
            <span> 
                &copy; 2021 Copyright Cerema & Simplon
            </span>
        </div>
        
    </footer>

</body>
</html>