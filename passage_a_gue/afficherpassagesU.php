
<?php
require_once "libraries/models/Users.php";
require_once "libraries/models/Passages.php";

$modelUser = new Users();
$modelPassage = new Passages();
$passages = $modelPassage->selectPassage();
$passage1 = $modelPassage->selectLanLat();
$passage1 = json_encode($passage1, true);

session_name('user_login');
session_start();
if (!isset($_SESSION["user_login"])) {
    header("Location: index.php");
}
$id = $_SESSION["user_login"];
$user = $modelUser->selectUserByid($id);


?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Le site propose...">
    <meta name="keywords" content="Passage, Gue, Route">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Passag à gué</title>
    <!-- Render all elements normaly -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- font awsome library -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- main template css file -->
    <link rel="stylesheet" href="css/index.css">
    <!-- header -->
    <link rel="stylesheet" href="css/header-user.css">
    <!-- main heading  -->
    <link rel="stylesheet" href="css/main-heading.css">
    <link rel="stylesheet" href="css/ajouterpassage.css">
    <!-- footer -->
    <link rel="stylesheet" href="css/footer.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Google font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <link rel="stylesheet" type="text/css" href="css/afficherpassagesU.css" />
    <script src="js/mapu.js" defer></script>
    <script src="js/triage.js" defer></script>   
   
</head>

<body>
    <header>
    <div class="header-user">
            <div class="container">
                <img class="logo" src="images/logo.png" alt="">
                <h3>Bienvenu <?php echo $user['prenom']; ?></h3>
                <div class="links">
                    <nav>
                        <i class="fas fa-bars menu"></i>
                        <ul>
                            <li><a href="indexuser.php">Home</a></li>
                            <li><a href="userprofile.php">Profile</a></li>
                            <li><a href="indexuser.php#contact">Contact</a></li>
                            <li><a href="logout.php">Desconnection</a></li>
                        </ul>
                    </nav>
                </div>
                <?php echo "<img src='./upload/".$user['photo']."' width='70px' height = '70px' >"?>
            </div>
        </div>
    </header>
    <div id="map"></div>
    <div id="passage"><?php echo $passage1 ?> </div>
    <div class="container">
        <div class="main-heading">
            <h2>Nos Passages</h2>
        </div>
        <div id="filter">
            <select id = "type" name="type" class="dropdown" onchange = "triParType('afficherpassageU.php?id_passage=')" >
                 <option value="">--TRIER PAR TYPE--</option>
                 <option value="Pierres">Pierres</option>
                 <option value="Sols">Sols</option>
                 <option value="Chaussée immergée">Chaussée immergée</option>
                 <option value="Pierres de gué">Pierres de gué</option>
                 <option value="Radier maçonnée">Radier maçonnée</option>
                 <option value="Chaussée béton">Chaussée béton</option>
                 <option value="Chaussée asphaltée">Chaussée asphaltée</option>
            </select>
            <button id = "gest" onclick = "triParGest('afficherpassageU.php?id_passage=')" class='btn btn-secondary bouton' value = <?php echo $user['id']; ?>>Afficher mes passages</button>
            <select id = "loc" name="type" class="dropdown" onchange = "triParLoc('afficherpassageU.php?id_passage=')" >
                 <option value="">--TRIER PAR LOCALISATION--</option>
                 <option value="04 -  Alpes-de-Haute-Provence">04 -  Alpes-de-Haute-Provence</option>
                 <option value="05 - Hautes-Alpes">05 - Hautes-Alpes</option>
                 <option value="06 - Alpes-Maritimes">06 - Alpes-Maritimes</option>
                 <option value="13 - Bouches-du-Rhône">13 - Bouches-du-Rhône</option>
                 <option value="83 - Var">83 - Var</option>
                 <option value="84 - Vaucluse">84 - Vaucluse</option>
            </select>
            </div>
     </br>       
    <div id="passages">
              <?php
                  if(count($passages) > 0) {
                      
                     echo "<table class='table table-striped table-hover'>
                     <tr>
                         <th>N°</th>
                         <th>Gps x</th>
                         <th>Gps y</th>
                         <th>Type</th>
                         <th>Localisation</th>
                         <th>Gestionaire</th>
                         <th>Voir le passage</th>
                     </tr>
                     ";
                     $i = 1;
                     foreach ($passages as $passage ) {
                         echo "
                             <tr>
                                 <td>".$i."</td>
                                 <td>".$passage['gps_x']."</td>
                                 <td>".$passage['gps_y']."</td>
                                 <td>".$passage['type_ouvrage']."</td>
                                 <td>".$passage['localisation']."</td>
                                 <td>".$passage['prenom']." ".$passage['nom']."</td>
                                 <td><a href='afficherpassageU.php?id_passage=".$passage['id_passage']."' class='btn btn-secondary bouton'>Voir le passage</a></td>
                             </tr>
                         ";
                         $i++;
                       } 
                       echo "</table>";
                    }
              ?>
        </div>
    </div>
   
    <footer>
        <div class="footer">
            <div>
                <a href="mentionsLegales.html" target="_blank" class="btn btn-secondary bouton">Mentions légales</a>
                
            </div>
            <br>
            <span> 
                &copy; 2021 Copyright Cerema & Simplon
            </span>
        </div>
        
    </footer>
    <script
      src="https://maps.googleapis.com/maps/api/js?callback=initMap&v=weekly"
      async
    ></script> 
</body>
</html>