<?php

require_once "libraries/utiles.php";
require_once "libraries/models/Users.php";
require_once "libraries/models/Passages.php";
require_once "libraries/models/Type.php";
require_once "libraries/models/Cours.php";
require_once "libraries/models/Localisation.php";


$modelUser = new Users();
$modelPassage = new Passages();
$modelType = new Type();
$modelCours = new Cours();
$modelLocalisation = new Localisation();

session_name('user_login');
session_start();
if (!isset($_SESSION["user_login"])) {
    header("Location: index.php");
}
$id = $_SESSION["user_login"];
$user = $modelUser->selectUserByid($id);
$type = $modelType->selectItem();
$cours = $modelCours->selectItem();
$local = $modelLocalisation->selectItem(); 


if (isset($_REQUEST['enregistrer']))  
{          
     // prendre les informations saisi par l'utilisateur et les mettre dans des variables  
     $gps_x = strip_tags($_REQUEST['gps_x']);
     $gps_y = strip_tags($_REQUEST['gps_y']);
     $frequence_sub = strip_tags($_REQUEST['frequence_sub']);
     $longeur = strip_tags($_REQUEST['longeur']);
     $largeur = strip_tags($_REQUEST['largeur']);
     $trafic_journalier = strip_tags($_REQUEST['trafic_journalier']);
     $taux_accidentalite = strip_tags($_REQUEST['taux_accidentalite']);
     $type_ouvrage = strip_tags($_REQUEST['type_ouvrage']);
     $cours_eau = strip_tags($_REQUEST['cours_eau']);
     $localisation = strip_tags($_REQUEST['localisation']);
     $gestionaire = $id;
     $errorMsg = verficationp($gps_x, $gps_y, $frequence_sub, $longeur, $largeur,$extension1, $extensions, $size1, $maxSize, $error1,$tmpName1, $name1);
  if (!isset($errorMsg))
    {	
     try
      {	
        $file = $modelPassage->uploadimageP($id_passage, $name1, $extension1, $tmpName1);

        $modelPassage->insertPassage($gps_x, $gps_y, $frequence_sub, $longeur, $largeur, $trafic_journalier,
        $taux_accidentalite,$type_ouvrage,$cours_eau,$localisation,$gestionaire, $file);
        $registerMsg="Vous avez bien ajouté votre passage"; //execute query success message
        
       }
       catch(PDOException $e)
       {
        echo $e->getMessage();
       }
   }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Le site propose...">
    <meta name="keywords" content="Passage, Gue, Route">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Passag à gué</title>
    <!-- Render all elements normaly -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- font awsome library -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- main template css file -->
    <link rel="stylesheet" href="css/index.css">
    <!-- header -->
    <link rel="stylesheet" href="css/header-user.css">
    <!-- main heading  -->
    <link rel="stylesheet" href="css/main-heading.css">
    <link rel="stylesheet" href="css/ajouterpassage.css">
    <!-- footer -->
    <link rel="stylesheet" href="css/footer.css">
    <!-- Google font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
</head>

<body>
     <!-- Start Header -->
    <header>
        <div class="header-user">
            <div class="container">
                <img class="logo" src="images/logo.png" alt="">
                <h3>Bienvenu <?php echo $user['prenom']; ?></h3>
                <div class="links">
                    <nav>
                        <i class="fas fa-bars menu"></i>
                        <ul>
                            <li><a href="indexuser.php">Home</a></li>
                            <li><a href="userprofile.php">Profile</a></li>
                            <li><a href="indexuser.php#contact">Contact</a></li>
                            <li><a href="logout.php">Desconnection</a></li>
                        </ul>
                    </nav>
                </div>
                <?php echo "<img src='./upload/".$user['photo']."' width='70px' height = '70px' >"?>
            </div>
        </div>
    </header>
    <!-- End Header -->
    <!-- start landing -->
    <div id="landing">
    <div class="containerall">
            <?php $modelPassage->error($errorMsg, $registerMsg); ?>
            <h2>AJOUTER UN PASSAGE</h2>
            <div class="container1">
                <div id="cont">
                    <form method="post" enctype = "multipart/form-data">
                    <label class="gps_x1" for="gps_x1">Gps_x:</label>
                    <input class="gps_x" type="integer" name="gps_x">
                    <label class="gps_y1" for="gps_y1">Gps_y:</label>
                    <input class="gps_y" type="integer" name="gps_y">
                    <label class="frequence_sub1" for="frequence_sub1">Frequence sub:</label>
                    <input class="frequence_sub" type="integer" name="frequence_sub">
                    <label class="longeur1" for="longeur1">Longeur:</label>
                    <input class="longeur" type="integer" name="longeur" placeholder="Longeur en m" >
                    <label class="largeur1" for="largeur1">Largeur:</label>
                    <input class="largeur" type="integer" name="largeur" placeholder="Largeur en m" >         
                </div>
                <div id="cont">
                    <label class="trafic_journalier1" for="trafic_journalier1">Trafic journalier:</label>
                    <input class="trafic_journalier" type="text" name="trafic_journalier">
                    <label class="taux_accidentalite1" for="taux_accidentalite1">Taux accidentalite:</label>
                    <input class="taux_accidentalite" type="integer" name="taux_accidentalite">
                    <label class="type_ouvrage1" for="type_ouvrage1">Type ouvrage:</label>
                    <select class="type_ouvrage" name="type_ouvrage" >
                        <option value="seltype">--chosisssez un type d'ouvrage--</option>
                        <?php  foreach ($type as $types ) {  ?>
                        <option value ="<?php echo $types["label"]; ?>"><?php echo $types["label"]; ?></option>
                        <?php } ?>
                    </select>                    
                    <label class="cours_eau1" for="cours_eau1">Cours eau:</label>
                    <select class="cours_eau" name="cours_eau" >
                        <option value="selcours">--chosisssez le cours d'eau--</option>
                        <?php  foreach ($cours as $cours1 ) {  ?>
                        <option value ="<?php echo $cours1["label"]; ?>"><?php echo $cours1["label"]; ?></option>
                        <?php } ?>
                    </select> 
                    <label class="localisation1" for="localisation1">Localisation:</label>
                    <select class="localisation" name="localisation" >
                        <option value="sellocal">--chosisssez un département--</option>
                        <?php  foreach ($local as $locals ) {  ?>
                        <option value ="<?php echo $locals["label"]; ?>"><?php echo $locals["label"]; ?></option>
                        <?php } ?>
                    </select>                 
                </div>
            </div>   <!-- ATTENTION, DIV CONTAINER1  -->        
            <div id="cont-photo">  <!-- J'AI CHANGÉ DIV NAME -->
                        <label class="photo" for="photo">Photo:</label>
                        <input class="file" type="file" name="passageimage">
            </div>
            <div id="cont-btn"> <!-- J'AI CHANGÉ DIV NAME -->
                        <input class = "submit" type="submit" name="enregistrer" value="Enregistrer">
            </div>           
        </div>
    </div>
    
    <!--    End landing -->
    <footer>
        <div class="footer">
            <div>
            <a href="mentionsLegales.html" target="_blank" class="btn btn-secondary bouton">Mentions légales</a>
            </div>
            <br>
            <span> 
                &copy; 2021 Copyright Cerema & Simplon
            </span>
        </div>
        
    </footer>

</body>
</html>