
function triParType(rep) {
let type_passage = document.getElementById('type').value;
document.querySelector('#passages').innerText = '';
let table = document.getElementById('passages');
table.className = 'table table-striped table-hover';
let tr = document.createElement('tr');
let th1 = document.createElement('th');
let th2 = document.createElement('th');
let th3 = document.createElement('th');
let th4 = document.createElement('th');
let th5 = document.createElement('th');
let th6 = document.createElement('th');
let th7 = document.createElement('th');

th1.innerText = "N°";
th2.innerText = "Gps_x";
th3.innerText = "Gps_y";
th4.innerText = "Type";
th5.innerText = "Localisation";
th6.innerText = "Gestionaire";
th7.innerText = "Voir le passage";

table.appendChild(tr);
tr.appendChild(th1);
tr.appendChild(th2);
tr.appendChild(th3);
tr.appendChild(th4);
tr.appendChild(th5);
tr.appendChild(th6);
tr.appendChild(th7);


i = 1;
// Créer un nouveau objet URLSearchParams pour ajouter le type_ouvrage au URL
// afin de le récuperer par la requête dans la page triageType.php
// let searchParams =  new URLSearchParams({type_ouvrage : `${type_passage}`});
// let url = 'libraries/triageType.php?' + searchParams.toString();
let url = 'libraries/triageType.php?type_ouvrage=' + type_passage;

fetch(url)
.then(function(response) { return response.json()})
.then(data => data.forEach(passage => {
    let tr1 = document.createElement('tr');
    let td1 = document.createElement('td');
    let td2 = document.createElement('td');
    let td3 = document.createElement('td');
    let td4 = document.createElement('td');
    let td5 = document.createElement('td');
    let td6 = document.createElement('td');
    let td7 = document.createElement('td');
    let btn = document.createElement('a');
    btn.href = rep+ passage.id_passage;
    btn.className = 'btn btn-secondary bouton';
    btn.innerText = 'Voir le passage';

    td1.innerText = i;
    td2.innerText = passage.gps_x;
    td3.innerText = passage.gps_y;
    td4.innerText = passage.type_ouvrage;
    td5.innerText = passage.localisation;
    td6.innerText = passage.prenom + ' ' + passage.nom;
    td7 = btn;

    table.appendChild(tr1);
    tr1.appendChild(td1);
    tr1.appendChild(td2);
    tr1.appendChild(td3);
    tr1.appendChild(td4);
    tr1.appendChild(td5);
    tr1.appendChild(td6);
    tr1.appendChild(td7);

i++;
}))}


function triParGest(rep) {
    let gestionaire = document.getElementById('gest').value;
    document.querySelector('#passages').innerText = ''
    let table = document.getElementById('passages');
    table.className = 'table table-striped table-hover';
    let tr = document.createElement('tr');
    let th1 = document.createElement('th');
    let th2 = document.createElement('th');
    let th3 = document.createElement('th');
    let th4 = document.createElement('th');
    let th5 = document.createElement('th');
    let th6 = document.createElement('th');
    let th7 = document.createElement('th');
    
    th1.innerText = "N°";
    th2.innerText = "Gps_x";
    th3.innerText = "Gps_y";
    th4.innerText = "Type";
    th5.innerText = "Localisation";
    th6.innerText = "Gestionaire";
    th7.innerText = "Voir le passage";
    
    table.appendChild(tr);
    tr.appendChild(th1);
    tr.appendChild(th2);
    tr.appendChild(th3);
    tr.appendChild(th4);
    tr.appendChild(th5);
    tr.appendChild(th6);
    tr.appendChild(th7);
    
    
    i = 1;
    // Créer un nouveau objet URLSearchParams pour ajouter le id_gestionaire au URL
    // afin de le récuperer par la requête dans la page triageGest.php
    // let searchParams =  new URLSearchParams({id_gestionaire : `${gestionaire}`});
    // let url = 'libraries/triageGest.php?' + searchParams.toString();
    let url = 'libraries/triageGest.php?id_gestionaire=' + gestionaire;

    fetch(url)
    .then(function(response) { return response.json()})
    .then(data => data.forEach(passage => {
        let tr1 = document.createElement('tr');
        let td1 = document.createElement('td');
        let td2 = document.createElement('td');
        let td3 = document.createElement('td');
        let td4 = document.createElement('td');
        let td5 = document.createElement('td');
        let td6 = document.createElement('td');
        let td7 = document.createElement('td');
        let btn = document.createElement('a');
        btn.href = rep+ passage.id_passage;
        btn.className = 'btn btn-secondary bouton';
        btn.innerText = 'Voir le passage'
    
        td1.innerText = i;
        td2.innerText = passage.gps_x;
        td3.innerText = passage.gps_y;
        td4.innerText = passage.type_ouvrage;
        td5.innerText = passage.localisation;
        td6.innerText = passage.prenom + ' ' + passage.nom;
        td7 = btn;
    
        table.appendChild(tr1);
        tr1.appendChild(td1);
        tr1.appendChild(td2);
        tr1.appendChild(td3);
        tr1.appendChild(td4);
        tr1.appendChild(td5);
        tr1.appendChild(td6);
        tr1.appendChild(td7);
    
    i++;
    }))}

    function triParLoc(rep) {
        let loc = document.getElementById('loc').value;
        document.querySelector('#passages').innerText = ''
        let table = document.getElementById('passages');
        table.className = 'table table-striped table-hover';
        let tr = document.createElement('tr');
        let th1 = document.createElement('th');
        let th2 = document.createElement('th');
        let th3 = document.createElement('th');
        let th4 = document.createElement('th');
        let th5 = document.createElement('th');
        let th6 = document.createElement('th');
        let th7 = document.createElement('th');
        
        th1.innerText = "N°";
        th2.innerText = "Gps_x";
        th3.innerText = "Gps_y";
        th4.innerText = "Type";
        th5.innerText = "Localisation";
        th6.innerText = "Gestionaire";
        th7.innerText = "Voir le passage";
        
        table.appendChild(tr);
        tr.appendChild(th1);
        tr.appendChild(th2);
        tr.appendChild(th3);
        tr.appendChild(th4);
        tr.appendChild(th5);
        tr.appendChild(th6);
        tr.appendChild(th7);
        
        
        i = 1;
        // Créer un nouveau objet URLSearchParams pour ajouter le type_ouvrage au URL
        // afin de le récuperer par la requête dans la page triageType.php
        // let searchParams =  new URLSearchParams({localisation : `${loc}`});
        // let url = 'libraries/triageLoc.php?' + searchParams.toString();
        let url = 'libraries/triageLoc.php?localisation=' + loc;

        fetch(url)
        .then(function(response) { return response.json()})
        .then(data => data.forEach(passage => {
            let tr1 = document.createElement('tr');
            let td1 = document.createElement('td');
            let td2 = document.createElement('td');
            let td3 = document.createElement('td');
            let td4 = document.createElement('td');
            let td5 = document.createElement('td');
            let td6 = document.createElement('td');
            let td7 = document.createElement('td');
            let btn = document.createElement('a');
            btn.href = rep+ passage.id_passage;
            btn.className = 'btn btn-secondary bouton';
            btn.innerText = 'Voir le passage'
        
            td1.innerText = i;
            td2.innerText = passage.gps_x;
            td3.innerText = passage.gps_y;
            td4.innerText = passage.type_ouvrage;
            td5.innerText = passage.localisation;
            td6.innerText = passage.prenom + ' ' + passage.nom;
            td7 = btn;
        
            table.appendChild(tr1);
            tr1.appendChild(td1);
            tr1.appendChild(td2);
            tr1.appendChild(td3);
            tr1.appendChild(td4);
            tr1.appendChild(td5);
            tr1.appendChild(td6);
            tr1.appendChild(td7);
        
        i++;
        }))}
    
