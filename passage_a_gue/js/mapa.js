let map;

function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
    zoom: 5,
    center: { lat: 46.232192999999995, lng: 2.209666999999996 },
  });

  let passage = JSON.parse(document.getElementById('passage').innerHTML);
  for (m in passage) {
	let infoWind = new google.maps.InfoWindow(),marker,lat,lng;
	lat = passage[ m ].gps_x;
	lng=passage[ m ].gps_y;
	let content = document.createElement('div');
	let div1 = document.createElement('div');
	let img = document.createElement('img');
	img.src = 'upload/passage/'+passage[m].photop;
	img.style.width = '100px';
	let strong = document.createElement('strong');
	let voir = document.createElement('a');
	voir.textContent = "Voir le passage";
	voir.href = "afficherpassageA.php?id_passage=" + passage[m].id_passage;
	strong.textContent = passage[m].type_ouvrage;
	div1.style.display = "grid";
	div1.style.textAlign = "center";
	div1.appendChild(strong);
	div1.appendChild(voir);
	content.appendChild(img);
	content.appendChild(div1);

	marker = new google.maps.Marker({
		position: new google.maps.LatLng(lat,lng),
		map: map
	});
	    
		marker.addListener('click', function(){
		infoWind.setContent(content);
		infoWind.open(map, marker);
	});
	
	
  }

}
