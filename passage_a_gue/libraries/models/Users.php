<?php
require_once ('libraries/models/Model.php');

class Users extends Model
{
    protected $table = "users";


    public function selectuser(string $lastname, string $email) {
        $select_stmt= $this->$db->prepare("SELECT * FROM users WHERE nom=:ulastname OR email=:uemail"); // sql select query
        $select_stmt->execute(array(':ulastname'=>$lastname, ':uemail'=>$email)); //execute query 
        $row=$select_stmt->fetch(PDO::FETCH_ASSOC);	
        return $row;
    }
    
    public function insertuser($firstname, $lastname, $email, $new_password, $file):void {
        $insert_stmt= $this->$db->prepare("INSERT INTO users (id,prenom,nom,email,mot_de_passe,photo) VALUES
                                            ('',:ufirstname,:ulastname,:uemail,:upassword, :photo)"); 		//sql insert query					
                
        $insert_stmt->execute(array(	':ufirstname'	=>$firstname, 
                                         ':ulastname' =>$lastname, 
                                         ':uemail'	=>$email, 
                                         ':upassword'=>$new_password,
                                         ':photo'=>$file));
    }
    
    public function updateuser($id, $firstname, $lastname, $email, $new_password) {
        $update_stmt= $this->$db->prepare("UPDATE users SET id = :uid,  prenom = :ufirstname ,nom = :ulastname, email = :uemail,
        mot_de_passe = :upassword where users.id = :uid");				
        $update_stmt->execute(array(	":uid" => $id,
                                        ":ufirstname"=>$firstname, 
                                         ":ulastname" =>$lastname, 
                                         ":uemail"	=>$email, 
                                         ":upassword"=>$new_password));
    }

    public function selectUserByid(int $id) {
        $select_stmt = $this->$db->prepare("SELECT * from {$this->table} WHERE id=:id");
        $select_stmt->execute(array(":id"=>$id));
        $user=$select_stmt->fetch(PDO::FETCH_ASSOC);
        return $user;
    }
    

  
    
}


?>