<?php

require_once ('libraries/models/Model.php');

class Passages extends Model
{

    protected $table = "passage";

    public function insertPassage($gps_x, $gps_y, $frequence_sub, $longeur, $largeur, $trafic_journalier,
        $taux_accidentalite,$type_ouvrage,$cours_eau,$localisation,$gestionaire, $file):void {
        $insert_stmt= $this->$db->prepare("INSERT INTO passage (id_passage,gps_x,gps_y,frequence_sub,longeur,
                                            largeur,trafic_journalier,taux_accidentalite,type_ouvrage,
                                            cours_eau, localisation, id_gestionaire, photop) VALUES
                                            ('',:gps_x,:gps_y,:frequence_sub,:longeur,
                                            :largeur,:trafic_journalier,:taux_accidentalite,:type_ouvrage,
                                            :cours_eau, :localisation, :gestionaire, :photo)"); 							
                
        $insert_stmt->execute(array(	':gps_x'	=>$gps_x, 
                                         ':gps_y' =>$gps_y, 
                                         ':frequence_sub'	=>$frequence_sub, 
                                         ':longeur'=>$longeur,
                                         ':largeur'=>$largeur,
                                         ':trafic_journalier'=>$trafic_journalier,
                                         ':taux_accidentalite'=>$taux_accidentalite,
                                         ':type_ouvrage'=>$type_ouvrage,
                                         ':cours_eau'=>$cours_eau,
                                         ':localisation'=>$localisation,
                                         ':gestionaire'=>$gestionaire,
                                         ':photo'=>$file));
    }

    public function selectPassage() {
        $select_stmt = $this->$db->prepare("SELECT * from passage join users on 
        passage.id_gestionaire = users.id order by passage.id_passage desc");
        $select_stmt->execute();
        $passage=$select_stmt->fetchall(PDO::FETCH_ASSOC);
        return $passage;
    }
    public function selectPassageR() {
        $select_stmt = $this->$db->prepare("SELECT * from passage join users on 
        passage.id_gestionaire = users.id order by passage.id_passage desc limit 3");
        $select_stmt->execute();
        $passage=$select_stmt->fetchall(PDO::FETCH_ASSOC);
        return $passage;
    }

    public function selectPassageByid(int $id) {
        $select_stmt = $this->$db->prepare("SELECT * from passage join users on 
        passage.id_gestionaire = users.id WHERE id_passage=:id");
        $select_stmt->execute(array(":id"=>$id));
        $passage=$select_stmt->fetch(PDO::FETCH_ASSOC);
        return $passage;
    }

    public function updatePassage($id_passage, $gps_x, $gps_y, $frequence_sub, $longeur, $largeur, $trafic_journalier,
        $taux_accidentalite,$type_ouvrage,$cours_eau,$localisation,$gestionaire):void {
        $update_stmt= $this->$db->prepare("UPDATE passage SET id_passage = :id, gps_x = :gps_x, gps_y = :gps_y,
                                            frequence_sub = :frequence_sub, longeur = :longeur,
                                            largeur = :largeur, trafic_journalier = :trafic_journalier,
                                            taux_accidentalite = :taux_accidentalite, type_ouvrage = :type_ouvrage,
                                            cours_eau = :cours_eau, localisation = :localisation WHERE id_passage = :id");							
                
        $update_stmt->execute(array(	 ':id'    =>$id_passage,
                                         ':gps_x'	=>$gps_x, 
                                         ':gps_y' =>$gps_y, 
                                         ':frequence_sub'	=>$frequence_sub, 
                                         ':longeur'=>$longeur,
                                         ':largeur'=>$largeur,
                                         ':trafic_journalier'=>$trafic_journalier,
                                         ':taux_accidentalite'=>$taux_accidentalite,
                                         ':type_ouvrage'=>$type_ouvrage,
                                         ':cours_eau'=>$cours_eau,
                                         ':localisation'=>$localisation));
    }

    public function deletePassage(int $id) {
        $delete_stmt = $this->$db->prepare("DELETE from passage WHERE id_passage=:id");
        $delete_stmt->execute(array(":id"=>$id));
    }

    public function uploadimageP($id, $name, $extension, $tmpName) {
        $uniqueName = uniqid('', true);
        $file1 = $uniqueName.".".$extension;
        move_uploaded_file($tmpName, './upload/passage/'.$file1);
        $update_stmt=$this->$db->prepare("UPDATE passage SET  photop = :photo where id_passage = :id");				
        $update_stmt->execute(array(":id" => $id, ":photo" => $file1));
        return $file1;
    }

    public function selectLanLat() {
        $select_stmt = $this->$db->prepare("SELECT id_passage, gps_x, gps_y, type_ouvrage, photop from passage");
        $select_stmt->execute();
        $passage1=$select_stmt->fetchall(PDO::FETCH_ASSOC);
        return $passage1;
    }

}

?>