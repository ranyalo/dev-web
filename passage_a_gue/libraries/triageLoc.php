<?php
    require_once "database.php";     
// Ce fichier va se connecter à la base de données, récupérer les données et les envoyer en json 

header('Content-type: application/json');
 $db = getdb();
 $loc = $_GET['localisation'];
try {
    $rqt = "SELECT * FROM passage join users on 
    passage.id_gestionaire = users.id WHERE passage.localisation = :loc;";
    $rqtPreparee = $db->prepare($rqt); 
    $rqtPreparee->bindParam('loc', $loc);
    $rqtPreparee->execute(); 
    $resultats = $rqtPreparee->fetchAll(PDO::FETCH_ASSOC);
    $resultats->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo json_encode($resultats);
} catch(Exception $e)  {
    echo json_encode(["error" => $e->getMessage()]);
}
?>