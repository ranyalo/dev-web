<?php

/**
 * une function pour connecter à la base de données
 * @return db
 */

function getdb() {
    $db_host = 'mysql-passageague.alwaysdata.net';
    $db_port = '3306';
    $db_user = '245046';
    $db_pass = '********';
    $db_name = 'passageague_projet';
   
    try {
    $db = new PDO("mysql:host=$db_host;dbport=$db_port;dbname=$db_name", $db_user, $db_pass);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch (PDOEXEPTION $e)
    {
        echo $e ->getMessage();
    }
    return $db;
}

?>
