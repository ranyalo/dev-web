<?php
    require_once "database.php";     
// Ce fichier va se connecter à la base de données, récupérer les données et les envoyer en json 

header('Content-type: application/json');
 $db = getdb();
 $type = $_GET['type_ouvrage'];
try {
    $rqt = "SELECT * FROM passage join users on 
    passage.id_gestionaire = users.id WHERE passage.type_ouvrage = :type;";
    $rqtPreparee = $db->prepare($rqt); 
    $rqtPreparee->bindParam('type', $type);
    $rqtPreparee->execute(); 
    $resultats = $rqtPreparee->fetchAll(PDO::FETCH_ASSOC);
    $resultats->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo json_encode($resultats);
} catch(Exception $e)  {
    echo json_encode(["error" => $e->getMessage()]);
}
?>