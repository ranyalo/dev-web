<?php
require_once "libraries/models/Users.php";
require_once "libraries/models/Passages.php";
$modelUser = new Users();
$modelPassage = new Passages();
$passages = $modelPassage->selectPassageR();

session_name('user_login');
session_start();

if (!isset($_SESSION["user_login"])) {
    header("Location: index.php");
}
$id = $_SESSION["user_login"];
$item = $modelUser->selectUserByid($id);
if ( $item['admin'] != "admin") {
    header("Location: index.php"); 
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Le site propose...">
    <meta name="keywords" content="Passage, Gue, Route">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Passag à gué</title>
    <!-- Render all elements normaly -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- font awsome library -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- main template css file -->
    <link rel="stylesheet" href="css/index.css">
    <!-- header -->
    <link rel="stylesheet" href="css/header-user.css">
    <!-- landing -->
    <link rel="stylesheet" href="css/landing.css">
    <!-- main heading  -->
    <link rel="stylesheet" href="css/main-heading.css">
    <!-- about-->
    <link rel="stylesheet" href="css/about.css">
    <!-- contact -->
    <link rel="stylesheet" href="css/contact.css">
    <!-- footer -->
    <link rel="stylesheet" href="css/footer.css">
    <!-- Google font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">  
</head>

<body>
     <!-- Start Header -->
     <header>
        <div class="header-user">
            <div class="container">
                <img class="logo" src="images/logo.png" alt="">
                <h3>Bienvenu <?php echo $item['prenom']; echo "</br> Adminstrateur" ?></h3>
                <div class="links">
                    <nav>
                        <i class="fas fa-bars menu"></i>
                        <ul>
                            <li><a href="indexadmin.php">Home</a></li>
                            <li><a href="adminprofile.php">Profile</a></li>
                            <li><a href="indexadmin.php#contact">Contact</a></li>
                            <li><a href="logout.php">Desconnection</a></li>
                        </ul>
                    </nav>
                </div>
                <?php echo "<img src='./upload/".$item['photo']."' width='70px' height = '70px' >"?>
            </div>
        </div>
     </header>
    <!-- End Header -->
    <!-- start landing -->
    <div id="landing">
        <div class="overlay"></div>
            <div class="text">
                <div class="content">
                    <h2>PASSAGE À GUÉ </h2>
                    <p>Ce site a été réalisé par les apprenants de Simplon.
                        Cette preuve de concept s'appuie sur une étude réalisée par le Cerema Méditerranée en liaison avec la Mission Interrégionale Inondation de l'Arc Méditerranéen.
                        L'objectif de ce projet est de proposer une application web permettant d'identifier et localiser les passages à gué en grand nombre dans les régions sud de la France et permettre, par une classification commune de mieux organiser et mettre en place une signalisation routière adaptée et assurer un suivi d'exploitation optimisé.</p>
                </div>
            </div>
        </div>
    <!--    End landing -->
    <!-- start about-->
    <div id="about">
        <div class="container">
            <div class="main-heading">
                <h2>Nos Passages</h2>
            </div>
            
            <ul class="shuffle">
                <li><a href="ajouterpassageA.php" class="ajout">Ajouter un passage</a></li>
                <li><a href="affichermodifier.php" class="affiche" >Afficher et modifier les passages</a></li>
            </ul>
            <div id="about-content">
              <!-- Afficage des trois passages les plus récents -->
            <?php
            foreach ($passages as $passage ) {
                echo "
                <div class = 'card'>
                    <a href = 'afficherpassageA.php?id_passage=" .$passage['id_passage']."'>
                    <img src= upload/passage/".$passage['photop']."  alt=''> </a>
                    <div class='info'>
                        <h3>" .$passage['type_ouvrage']. "</h3>
                        <h3>" .$passage['localisation']. "</h3>
                    </div>
                </div> ";} ?>
            </div>
        </div>
    </div>
    <!-- End about -->
    <div id="contact">
        <div class="container">
                <div class="main-heading">
                    <h2 class="special-heading">Contact</h2>
                </div>
                <div class="info">
                    <p class="label"> Nos Corrdonnées:</p>
                    <a href="mailto:passage@gmail.com" class="link"> E-mail: passage@gmail.com</a>
                    <a href="tel:+337xxxxxx" class="link">tel: +33 7 43 28 56 00</a>
                    <div class="social">
                        Reseaux socieaux :
                        <i class="fab fa-facebook-f"></i>
                        <i class="fab fa-twitter"></i>
                    </div>
                </div>

            </div>
        </div>
    <!-- start logos -->
    <logos>
        <div>
            <a href="#">
                <figure>
                    <img src="/images/MIIAM_logo2.jpg" alt="Logotype MIIAM">
                </figure>
            </a>
            <a href="https://www.cerema.fr/">
                <figure>
                    <img src="images/LogoCerema_horizontal.svg" alt="Logotype Cerema">
                </figure>
            </a>
            <a href="https://www.afpa.fr/">
                <figure>
                    <img src="/images/LogoAfpa.jpg" alt="Logotype Afpa">
                </figure>
            </a>
        </div> 
    </logos>
    <!-- End logos -->

    <footer>
        <div class="footer">
            <div>
                <a href="mentionsLegales.html" target="_blank" class="btn btn-secondary bouton">Mentions légales</a>
                
            </div>
            <br>
            <span> 
                &copy; 2021 Copyright Cerema & Simplon
            </span>
        </div>
        
    </footer>
    
</body>
</html>