<?php
require_once "libraries/models/Users.php";
require_once "libraries/models/Model.php";
require_once "libraries/utiles.php";

$modelUser = new Users();

session_name('user_login');
session_start();
if (!isset($_SESSION["user_login"])) {
    header("Location: index.php");
}
$id = $_SESSION["user_login"];
$item = $modelUser->selectUserByid($id);
if ( $item['admin'] != "admin") {
    header("Location: index.php"); 
}

if (isset($_REQUEST['supprimer']))  // 'supprimer' le nom de bouton
{
  $modelUser->delete($id);
  unlink('upload/'.$item['photo']);
  header("Location: index.php");
}

if (isset($_REQUEST['modifier']))  // 'modifier' le nom de bouton
{          
     // prendre les informations saisi par l'utilisateur et les mettre dans des variables  
     $firstname = strip_tags($_REQUEST['prenom']);
     $lastname = strip_tags($_REQUEST['nom']);
     $email = strip_tags($_REQUEST['email']);
     $password = strip_tags($_REQUEST['password']);
     $password_v = strip_tags($_REQUEST['password_v']);
     $errorMsg =  verfication($lastname, $email, $password, $password_v, $extension, $extensions, $size, $maxSize, $error, $tmpName, $name);
  if (!isset($errorMsg))
    {	
     try
      {	
            if (isset($_FILES['userimage']) AND !empty($name)) { // update la photo de profile
              unlink('upload/'.$item['photo']);                   // supprimer l'encien
              $modelUser->uploadimage($id, $name, $extension, $tmpName);}    // ajouter la nouvelle

            $new_password = password_hash($password, PASSWORD_DEFAULT); //encrypt le mot de passe en utilisant password_hash()
            
            $modelUser->updateuser($id, $firstname, $lastname, $email, $new_password); 
            $registerMsg = "Votre profile est modifier";
            header("refresh:1; adminprofile.php");
        
       }
       catch(PDOException $e)
       {
        echo $e->getMessage();
       }
   }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Le site propose...">
    <meta name="keywords" content="Passage, Gue, Route">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Passag à gué</title>
    <!-- Render all elements normaly -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- font awsome library -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- main template css file -->
    <link rel="stylesheet" href="css/index.css">
    <!-- main heading  -->
    <link rel="stylesheet" href="css/main-heading.css">
    <!-- header -->
    <link rel="stylesheet" href="css/header-user.css">
    <link rel="stylesheet" href="css/userprofile.css">
    <!-- footer -->
    <link rel="stylesheet" href="css/footer.css">
    <!-- Google font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
</head>

<body>
    <!-- Start Header -->
    <header>
        <div class="header-user">
            <div class="container">
                <img class="logo" src="images/logo.png" alt="">
                <h3>Bienvenu <?php echo $item['prenom']; echo "</br> Adminstrateur";?></h3>
                <div class="links">
                    <nav>
                        <i class="fas fa-bars menu"></i>
                        <ul>
                            <li><a href="indexadmin.php">Home</a></li>
                            <li><a href="adminprofile.php">Profile</a></li>
                            <li><a href="indexadmin.php#contact">Contact</a></li>
                            <li><a href="logout.php">Desconnection</a></li>
                        </ul>
                    </nav>
                </div>
                <?php echo "<img src='./upload/".$item['photo']."' width='60px' height = '60px' >"?>
            </div>
        </div>
     </header>
    <!-- End Header -->
    <div class="containerall">
        <?php $modelUser->error($errorMsg, $registerMsg); ?>
        <h2>MODIFIER MON COMPTE</h2>
        <div class="container1">
            <div id="cont">
                <form method="post" enctype = "multipart/form-data">
                <label class="nom" for="nom">Nom:</label>
                <input class="name" type="text" name="nom" value = "<?php echo $item['nom'];?>">
                <label class="prenom1" for="prenom">Prénom:</label>
                <input class="prenom" type="text" name="prenom" value = "<?php echo $item['prenom'];?>">
                <label class="email1" for="email">Email:</label>
                <input class="email" type="email" name="email" value = "<?php echo $item['email'];?>">
                <label class="pass" for="pass">Mot de passe:</label>
                <input class="password" type="password" name="password" placeholder="Nouveau mot de passe" >
                <label class="pass1" for="mot de passe">Confirmation:</label>
                <input class="password1" type="password" name="password_v" placeholder="Confirmation" >         
            </div>          
            <div id="cont">
                        <?php echo "<img src='./upload/".$item['photo']."' width='300px' height = '300px' ><br>"?>
                        <label class="photo" for="photo">Photo:</label>
                        <input class="file" type="file" name="userimage">
            </div>
            <div id="cont">
                        <input class = "submit" type="submit" name="modifier" value="Modifier">
                        <input class = "submit" type="submit" name="supprimer" value="Supprimer" onclick="return window.confirm(`Êtes vous sûr de vouloir supprimer votre compte ?!`)">
            </div>           
        </div> 
    </div>
    
<footer>
    <div class="footer">
        <div>
            <a href="mentionsLegales.html" target = "_blank" class="btn btn-secondary bouton">Mentions légales</a>
        </div>
        <span> 
            &copy; 2021 Copyright Cerema & Simplon
        </span>
    </div>
</footer>
</body>
</html>
