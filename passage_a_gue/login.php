<?php

require_once "libraries/models/Users.php";
$modelUser = new Users();
session_name('user_login');  // Une session est un moyen de stocker des 
session_start();             // informations (dans des variables) à utiliser sur plusieurs pages.

if(isset($_REQUEST['btn_login']))	//button nom est "btn_login" 
{
	$lastname	=strip_tags($_REQUEST["lastname_email"]);	
	$email		=strip_tags($_REQUEST["lastname_email"]);	
	$password	=strip_tags($_REQUEST["password"]);			
		
	if(empty($lastname)){						
		$errorMsg[]="veuillez entrer le nom d'utilisateur ou l'e-mail";	//check "nom/email" textbox n'est pas vide 
	}
	else if(empty($email)){
		$errorMsg[]="veuillez entrer le nom d'utilisateur ou l'e-mail";	//check "nom/email" textbox n'est pas vide
	}
	else if(empty($password)){
		$errorMsg[]="veuillez entrer le mot de pass";	//check "passowrd" textbox n'est pas vide
	}
	else
	{
		try
		{
			$row = $modelUser->selectuser($lastname, $email);
			
			if($row)	//check database n'est pas vide.
			{
				if($lastname==$row["nom"] OR $email==$row["email"]) //check condition user est existe par nom ou email.
				{
					if(password_verify($password, $row["mot_de_passe"])) //check condition user "password" est corrospendant avec celci de database "password" en utilisant password_verify()
					{
						$_SESSION["user_login"] = $row["id"];
						if ($row['admin'] == "user"){
						$loginMsg = "Connexion réussie...";	
						header("refresh:1; indexuser.php");		
						}
						else if ($row['admin'] == "admin") {
							$loginMsg = "Connexion réussie... Vous êtes adminstrateur";		
							header("refresh:1; indexadmin.php");		
						}
					}
					else
					{
						$errorMsg[]="Mauvais mot de passe";
					}
				}
				else
				{
					$errorMsg[]="Mauvais nom d'utilisateur ou e-mail";
				}
			}
			else
			{
				$errorMsg[]="Mauvais nom d'utilisateur ou e-mail";
			}
		}
		catch(PDOException $e)
		{
			$e->getMessage();
		}		
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Le site propose...">
    <meta name="keywords" content="Passage, Gue, Route">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Passag à gué</title>
    <!-- Render all elements normaly -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- font awsome library -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- main template css file -->
    <link rel="stylesheet" href="css/index.css">
    <!-- header -->
    <link rel="stylesheet" href="css/header.css">
    <!-- landing -->
    <link rel="stylesheet" href="css/landing.css">
	<link rel="stylesheet" href="css/login.css">
    <!-- main heading  -->
    <link rel="stylesheet" href="css/main-heading.css">
    <!-- portfolio -->
    <link rel="stylesheet" href="css/portfolio.css">
    <!-- footer -->
    <link rel="stylesheet" href="css/footer.css">
    <!-- Google font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">
    
    
</head>

<body>
    <header>
        <div class="container">
            <a href="" class="logo">
                <img src="images/logo.png" alt="logo">
            </a>
            <nav>
                <i class="fas fa-bars menu"></i>
                <ul>
                    <li><a href="index.php" class="home">Home</a></li>
                    <li><a href="index.php#contact" class="Contact">Contact</a></li>
                    <li><a href="inscription.php" class="inscription">Inscription</a></li>
                    <li><a href="login.php" class="connection">Connection</a></li>
                </ul> 
            </nav>
        </div>
    </header>

	<div id="landing">
	<div class="container2">		
		<?php
		if(isset($errorMsg))
		{
			foreach($errorMsg as $error)
			{
			?>
				<div class="alert-danger">
					<strong><?php echo $error; ?></strong>
				</div>
            <?php
			}
		}
		if(isset($loginMsg))
		{
		?>
			<div class="alert-success">
				<strong><?php echo $loginMsg; ?></strong>
			</div>
        <?php
		}
		?>   
			<form method="post">
			    <div id="connecter">
                  <h2>Connexion</h2>
                </div>
			    <hr></br>
                <label class="utilisateur" for="utilisateur">Utilisateur:</label>
                <input class="name" type="text" name="lastname_email" placeholder="Nom ou email" >
                <label class="password1" for="mot de passe">Mot de passe:</label>
                <input class="password" type="password" name="password" placeholder="Mot de passe" >
                <input class="submit" type="submit" name="btn_login" value="Me connecter">
                <hr>
				<div id = "connection">
				 <a href="inscription.php">Créer un nouveau compte</a>		
				</div>	
			</form>
			
	</div>
	</div>
	<footer>
        <div class="footer">
            <div>
                <a href="mentionsLegales.html" target = "_blank" class="btn btn-secondary bouton">Mentions légales</a>
            </div>
            <br>
            <span> 
                &copy; 2021 Copyright Cerema & Simplon
            </span>
        </div>
    </footer>							
	</body>
</html>
